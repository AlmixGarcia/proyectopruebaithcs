﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCCC.Utilities
{
    public enum UserRoles
    {
        None = 0,
        Student = 1,
        Coordinator = 2,
        SuperCoordinator = 3,
        Admin = 4
    }
}
