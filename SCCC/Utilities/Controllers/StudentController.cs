﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCCC.Models;
using SCCC.Context;
using SCCC.Utilities;
using System.Web.Security;

namespace SCCC.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index(int id,string name,int role)
        {
            ViewBag.Id = id;
            ViewBag.Name = name;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Models.UserModel user)
        {
            if (ModelState.IsValid)
            {
                if (isValid(user.UserId, user.Password))
                {
                    FormsAuthentication.SetAuthCookie(Convert.ToString(user.UserId), false);

                    using (var db = new SCCEntities())
                    {
                        var userDB = db.Users.FirstOrDefault(x => x.UserId == user.UserId);

                        switch (userDB.Role)
                        {
                            case 1:
                                return RedirectToAction("Index", "Student", new
                                {
                                    id = userDB.UserId,
                                    name = userDB.Name + " " + userDB.FirstLastName + " " + userDB.SecondLastName,
                                    role = userDB.Role
                                });
                            case 2:
                                return RedirectToAction("Index", "Home");
                            case 3:
                                return RedirectToAction("Index", "Home");
                            case 4:
                                return RedirectToAction("Index", "Home");
                            default:
                                break;
                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Error en el Inicio de Sesión, Verifica tus Datos.");
                }
            }
            return View(user);
        }

        private bool isValid(int userid, string password)
        {
            bool valid = false;

            using (var db = new SCCEntities())
            {
                var user = db.Users.FirstOrDefault(x => x.UserId == userid);

                if (user != null)
                {
                    if (user.Password == Cryptography.BCrypt(password, user.Salt))
                    {
                        valid = true;
                    }
                }
            }

            return valid;
        }



    }
}