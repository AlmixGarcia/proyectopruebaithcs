﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SCCC.Context;
using System.IO;

namespace SCCC.Controllers
{
    public class RequestsController : Controller
    {
        private SCCEntities db = new SCCEntities();

        // GET: Requests
        public ActionResult Index()
        {
            var requests = db.Requests.Include(r => r.Users);
            return View(requests.ToList());
        }

        // GET: Requests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null)
            {
                return HttpNotFound();
            }
            return View(requests);
        }

        // GET: Requests/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Name");
            return View();
        }

        // POST: Requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RequestId,Type,Reason,UserId")] Requests requests)
        {
            requests.Status = 1;
            requests.IsDeleted= false;
            requests.CreationDate=DateTime.Now;

            if (ModelState.IsValid)
            {
                
                db.Requests.Add(requests);
                db.SaveChanges();
                ViewBag.Requests = requests;
                switch (requests.Type)
                {
                    case 1:
                        return View("AddCourse");
                    case 2:
                        return View("DeleteCourse");
                    case 3:
                        return View("SpecialCourse");
                    case 4:
                        return View("SummerCourse");
                    case 5:
                        return View("DeleteSemester");
                    case 6:
                        return View("DeleteCourseOutTime");
                    case 7:
                        return View("DeleteSemesterOutTime");
                    case 8:
                        return View("Proof");
                    case 9:
                        return View("Move");
                    case 10:
                        return View("Message");
                    case 11:
                        return View("OtherReason");

                }
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", requests.UserId);
            return View(requests);
        }

        // GET: Requests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", requests.UserId);
            return View(requests);
        }

        // POST: Requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RequestId,Status,Type,Reason,IsDeleted,CreationDate,UserId")] Requests requests)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requests).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", requests.UserId);
            return View(requests);
        }

        // GET: Requests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Requests requests = db.Requests.Find(id);
            if (requests == null)
            {
                return HttpNotFound();
            }
            return View(requests);
        }

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Requests requests = db.Requests.Find(id);
            db.Requests.Remove(requests);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Justificante(Proofs Proofs)
        {


            return View();
        }
        [HttpPost]
        public ActionResult AddCourse(AddCourses addCourses)
        {
            if (ModelState.IsValid)
            {
                db.AddCourses.Add(addCourses);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult DeleteCourse(DeleteCourses deleteCourses)
        {
            if (ModelState.IsValid)
            {
                db.DeleteCourses.Add(deleteCourses);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult DeleteSemester(DeleteSemesters deleteSemester)
        {
            if (ModelState.IsValid)
            {
                db.DeleteSemesters.Add(deleteSemester);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("DeleteSemesterFile");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult SpecialCourse(SpecialsCourse specialCourse)
        {
            if (ModelState.IsValid)
            {
                db.SpecialsCourse.Add(specialCourse);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult SummerCourse(SummerCourses summerCourse)
        {
            if (ModelState.IsValid)
            {
                db.SummerCourses.Add(summerCourse);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult DeleteCourseOutTime(DeleteCoursesOutTime deleteCourseOutTime)
        {
            if (ModelState.IsValid)
            {
                db.DeleteCoursesOutTime.Add(deleteCourseOutTime);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DeleteSemesterOutTime(DeleteSemestersOutTime deleteSemesterOutTime)
        {
            if (ModelState.IsValid)
            {
                db.DeleteSemestersOutTime.Add(deleteSemesterOutTime);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Message(Messages message)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult OtherReason(OthersReason othersReason)
        {
            if (ModelState.IsValid)
            {
                db.OthersReason.Add(othersReason);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("Index");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Proof(Proofs proofs)
        {
            if (ModelState.IsValid)
            {
                db.Proofs.Add(proofs);
                db.SaveChanges();
                ViewBag.Requests = Request;
                return RedirectToAction("ProofFile");
                //ViewBag.UserId = new SelectList(db.Users, "UserId", "Name", addCourses.AddCourseID);
                //return View(addCourses);
            }

            //return View();
            return RedirectToAction("Index");
        }

        #region 'Proof Files'
        public ActionResult ProofFile()
        {
            return View();
        }
        public ActionResult ProofFileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Files/ProofFiles"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "Correcto";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Error en la Carga de Archivo";
            }
            return View();
        }




        #endregion

        #region'Delete Semester Files'
        public ActionResult DeleteSemesterFile()
        {
            return View();
        }
        public FileResult DeleteSemesterFileDownload()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\DocumentosAEnviar\Baja.doc");
            string fileName = "Baja.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        [HttpPost]
        public ActionResult DeleteSemesterFileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Files/DeleteSemesterFiles"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "Correcto";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Error en la Carga de Archivo";
            }
            return View();
        }
        #endregion

        #region 'Delete Course OutTime Files'
        public ActionResult DeleteCourseOutTimeFile()
        {
            return View();
        }
        public FileResult DeleteCourseOutTimeFileDownload()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\DocumentosAEnviar\Comite.doc");
            string fileName = "Baja.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        [HttpPost]
        public ActionResult DeleteCourseOutTimeFileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Files/AcademicCommitteeFiles"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "Correcto";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Error en la Carga de Archivo";
            }
            return View();
        }


        #endregion

        #region 'Delete Semester OutTime Files'
        public ActionResult DeleteSemesterOutTimeFile()
        {
            return View();
        }
        public FileResult DeleteSemesterOutTimeFileDownload()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\DocumentosAEnviar\Comite.doc");
            string fileName = "Baja.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        [HttpPost]
        public ActionResult DeleteSemesterOutTimeFileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Files/AcademicCommitteeFiles"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "Correcto";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Error en la Carga de Archivo";
            }
            return View();
        }


        #endregion

        #region 'Delete Course OutTime Files'
        public ActionResult DeleteCourseFile()
        {
            return View();
        }
        public FileResult DeleteCourseFileDownload()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"c:\DocumentosAEnviar\Comite.doc");
            string fileName = "Baja.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        [HttpPost]
        public ActionResult DeleteCourseFileUpload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Files/AcademicCommitteeFiles"),
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "Correcto";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Error en la Carga de Archivo";
            }
            return View();
        }


        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
