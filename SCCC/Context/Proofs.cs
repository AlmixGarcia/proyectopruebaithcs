//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SCCC.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class Proofs
    {
        public Proofs()
        {
            this.ProofDocuments = new HashSet<ProofDocuments>();
        }
    
        public int ProofId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public int RequestID { get; set; }
    
        public virtual ICollection<ProofDocuments> ProofDocuments { get; set; }
        public virtual Requests Requests { get; set; }
    }
}
