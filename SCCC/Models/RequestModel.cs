﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCCC.Models
{
    public class RequestModel
    {
        [Required]
        [Display(Name = "ID de Solicitud")]
        [Key]
        public int RequestId { get; set; }
        [Required]
        [Display(Name = "Tipo")]
        public string Type { get; set; }
        [Required]
        [Display(Name = "Estado")]
        public string Status { get; set; }
        [Required]
        [Display(Name = "Motivo")]
        public string Reason { get; set; }
        [Required]
        [Display(Name = "Fecha de Creación")]
        public DateTime CreationDate = DateTime.Now;

        [Required]
        [Display(Name = "ID de Usuario")]
        public int UserId { get; set; }
    }
}
